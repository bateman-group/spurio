import numpy as np
import os
from Bio import SeqIO

# Used to (half-manually) find the length of our nucleotide database.
# this is needed for evalue correction

db0 = '../../pipe/tblastn_bact/db/dbfilter2_part1/source/source.fa'
db1 = '../../pipe/tblastn_bact/db/dbfilter2_part2/source/source.fa'

db0s = SeqIO.parse(db0, 'fasta')
db1s = SeqIO.parse(db1, 'fasta')

l0 = np.sum(np.array([len(i) for i in db0s]))
l1 = np.sum(np.array([len(i) for i in db1s]))
l2 = l0 + l1
os.system('echo {} > dblens.txt'.format(l2))
