import os
import numpy as np

# Since the executer.py takes ages to process every single protein,
# and since the EBI has unlimited computing resources :), we can use this
# script to process a lot of proteins in parallel.

stepsize = 50
for v in [1]:#,3,5,10]:
    for i in np.arange(2000,7209,stepsize):
        #print('bsub \'python executer.py -s {} -e {}\''.format(int(i), int(i+stepsize)))
        os.system('bsub -M 10000 \'python executer.py -s {} -e {} -v {}\''.format(int(i), int(i+stepsize), v))
    #os.system('python executer.py -s {} -e {}'.format(int(i), int(i+1)))
