import matplotlib as mpl
mpl.use('Agg')
import numpy as np
import sys
import glob, pdb, os,ipdb
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc
import time

def onclick(event):
    ix, iy = event.xdata, event.ydata
    print("I clicked at x={0:5.2f}, y={1:5.2f}".format(ix,iy))

    # Calculate, based on the axis extent, a reasonable distance
    # from the actual point in which the click has to occur (in this case 5%)
    ax = plt.gca()
    dx = 0.01 * (ax.get_xlim()[1] - ax.get_xlim()[0])
    dy = 0.01 * (ax.get_ylim()[1] - ax.get_ylim()[0])

    # Check for every point if the click was close enough:
    for i in range(len(x)):
        if(x[i] > ix-dx and x[i] < ix+dx and y[i] > iy-dy and y[i] < iy+dy):
            print('Hit. Loading png...')
            filename = '../outfigs/'+spaflist[i].split('/')[2].split('.')[0]+'.png'
            #os.system('xdg-open {}'.format(filename))
            os.system('cp {} myselections'.format(filename))

def classify(pic_plus_len, roistart, roistop):
    '''
    Input: matrix of tblastn msa
    Output: Float value describing sanity of MSA.
    '''

    #Later: CD Hit or something to condense information
    pic = pic_plus_len[:,3:-20]
    startidx = pic_plus_len[:,0]
    stopidx = pic_plus_len[:,1]
    roi = [roistart,roistop]
    stopnumber = 2.
    tol = 0.10
    tolaa = 10#int(tol*len(pic[-1]))
    
    #hasstop 0: Fixed roi interval, e.g. [0.1, 0.9]
    #hasstop0 = [stopnumber in pic[i, int(roi[0]*len(pic[0])):int(roi[1]*len(pic[0]))] for i in range(len(pic))]
    
    #hasstop 1: Amino acid tolerance from body
    hasstop1 = [stopnumber in pic[i, int(startidx[i]+tolaa-1) : int(stopidx[i]-tolaa)] for i in range(len(pic))]
    
    #val0 = np.sum(hasstop0)/float(np.shape(pic)[0])
    #val1: stop per seq
    stop_per_seq = np.sum(hasstop1)/float(np.shape(pic)[0])
    #Now: n stops / n 'active' body 

    #Second attempt
    #n_stop = np.sum(pic == 2)
    #n_stop =np.sum(pic[:, int(roi[0]*len(pic[0])):int(roi[1]*len(pic[0]))] == 2)
    n_stop = np.sum([np.sum(pic[i, int(startidx[i]+tolaa-1) : int(stopidx[i]-tolaa)]==2) for i in range(len(pic))])
#    n_bodyaas = np.sum([np.sum(pic[i, int(startidx[i]+tolaa-1) : int(stopidx[i]-tolaa)] in (2,4)) for i in range(len(pic))])
 
    n_bodyaas = len(pic[pic==2]) + len(pic[pic==4]) #np.sum(pic[pic in (2,4)])
#    n_bodyaas = np.sum([(pic[i] == 2) or (pic[i] == 4) for i in range(len(pic))])
    stop_per_residue = n_stop / (float(n_bodyaas)+1)
    #print(val)

    # Return features used for classification: 
    # Total number of stops, Stops per amino acids in MSA, Seqs with at least 1 Stop, number of sequences, length of input sequence, amino acids in MSA
    return n_stop, stop_per_residue, stop_per_seq, np.shape(pic)[0], np.shape(pic)[1], float(n_bodyaas)


def run_classifier(spname, afname, roistart, roistop, use_saved = 0, benchmark = 0, eval_cutoff = '0', sptype = 'spafs'):
    if use_saved == 0:

        n_features = 6
        datadir = '../pic_output_'+eval_cutoff+'/'
        splist = glob.glob(datadir+spname+'_*.npy')
        aflist = glob.glob(datadir+afname+'_*.npy')
        #we want balanced sets! even if it costs sth. 
        minlen = np.min((len(splist), len(aflist)))
        splist = splist[:minlen]
        aflist = aflist[:minlen]
         
        spvals = np.ones((len(splist), n_features + 1))
        afvals = np.zeros((len(aflist), n_features + 1))
        spaflist = []
        numbers = np.zeros(len(afvals))
#        for n, c in enumerate(aflist):
#            numb = int(c.split('_')[3])
#            numbers[n] = numb
#        pdb.set_trace()
        for n, c in enumerate(splist):
            pic = np.load(c)
            print(n)
            if not np.shape(pic)[0] == 0:
                spvals[n, 0:6] = classify(pic, roistart, roistop)
                spaflist.append(c)
            else:
                spvals[n,0:n_features] = np.zeros(n_features)
                spaflist.append(c)
                #print('Elapsing empty ding')
                pass
    	    #spvals[n, 3] = c


        for n, c in enumerate(aflist):
            pic = np.load(c)
            print(n)
            if not np.shape(pic)[0] == 0:
                afvals[n, 0:n_features] = classify(pic, roistart, roistop)
                spaflist.append(c)
            else:
                afvals[n,0:n_features] = np.zeros(n_features)
                spaflist.append(c)
                pass



            #afvals[n, 3] = c


        bothvals = np.vstack((spvals, afvals))
        np.save('cache/spvals_{}_{}_{}'.format(eval_cutoff,spname, afname), spvals)
        np.save('cache/afvals_{}_{}_{}'.format(eval_cutoff,spname, afname), afvals)
        np.save('cache/bothvals_{}_{}_{}'.format(eval_cutoff,spname,afname), bothvals)
        np.save('cache/spaflist_{}_{}_{}'.format(eval_cutoff,spname,afname), spaflist)
    else:
        spvals = np.load('cache/spvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
        afvals = np.load('cache/afvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
        bothvals = np.load('cache/bothvals_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))
        spaflist = np.load('cache/spaflist_{}_{}_{}.npy'.format(eval_cutoff,spname,afname))

        #print('##WARNING## Using preprocessed results from an earlier run. If you want to calculate the graphs freshly, set use_saved to 0.')
        #bothvals = np.load('bothvals.npy')
        #spaflist = np.load('spaflist.npy')
        #spvals = np.load('spvals.npy')
        #afvals = np.load('afvals.npy')

    ### We have our data. Now: Calc roc curve and PLOT THINGS! ###
    #empty, i.e. unclassifyable
    sane = np.where(bothvals[:,2] > 0)
    sanesp = np.where(spvals[:,2] > 0)
    saneaf = np.where(afvals[:,2] > 0)
    x = bothvals[:,0]#[sane]
    y = bothvals[:,2]#[sane]

    #bv2, bv3, bv4 = bothvals[:,2][sane], bothvals[:,3][sane], bothvals[:,4][sane]
    bv2, bv3, bv4 = bothvals[:,2], bothvals[:,3], bothvals[:,4]


    x1 = spvals[:,0]#[sanesp]
    y1 = spvals[:,2]#[sanesp]

    x2 = afvals[:,0]#[saneaf]
    y2 = afvals[:,2]#[saneaf]

    y12 = spvals[:,1]
    y22 = afvals[:,1]
    xperres1 = spvals[:,3]#[sanesp]
    xperres2 = afvals[:,3]#[saneaf]

    #pdb.set_trace()
    worst = np.argsort(x1)[-20:]
    badpics = spaflist[worst] 
    for p in badpics:
        p2 = '../outfigs_1/'+p.split('/')[2][:-4]+'.png'
        os.system('cp {} myselections'.format(p2))
    print(badpics)
    print(np.sort(x1)[-20:])
    p99 = np.percentile(x1,97)
    af_catch1 = len(np.where(x2>p99)[0])/len(x2)
    p99 = np.percentile(xperres1,97)
    af_catch2 = len(np.where(xperres2>p99)[0])/len(xperres2)


    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    fpr, tpr, _ = roc_curve(1-bv4.astype(int), x)
    roc_auc = auc(fpr, tpr)

    fpr2, tpr2, _ = roc_curve(1-bv4.astype(int), bv3)
    roc_auc2 = auc(fpr2, tpr2)

    fpr3, tpr3, _ = roc_curve(1-bv4.astype(int), bv2)
    roc_auc3 = auc(fpr3, tpr3)
    print('AUC bodycut perresidue: ',roc_auc)
    print('AUC bodycut persequence: ',roc_auc2)
    print('perresidue 97th percentile: ',af_catch1)
    print('persequence 97th percentile: ',af_catch2)
    if benchmark:
        print('Done. AUC: ', roc_auc)
        return roc_auc
    plt.figure()
    plt.plot(fpr, tpr, color = 'darkorange', label = "ROC curve (area = $02f)" % roc_auc)
    plt.plot([0,1],[0,1],color = 'navy', linestyle = '--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('AUC: {}, AF safecatch: {}'.format(round(roc_auc, 2), af_catch1))
    plt.savefig('rocs/Roccurve_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype))
    plt.close()

    plt.figure()
    plt.plot(fpr2, tpr2, color = 'darkorange', label = "ROC curve (area = $02f)" % roc_auc)
    plt.plot([0,1],[0,1],color = 'navy', linestyle = '--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('AUC: {}, AF safecatch: {}'.format(round(roc_auc2, 2), af_catch2))
    plt.savefig('rocs/tolborderRoccurve_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype))
    plt.close()

    ### Plot our fancy interactive scatterplot ###

    fig = plt.figure()
    ax  = fig.add_subplot(111)
    #ax.plot(x,y, 'o', c = bothvals[:,3])
    ax.scatter(x1,y12, label = 'Swissprot', s = 0.01)

    ax.scatter(x2,y22, label = 'AntiFam hits (TREMBL)', s = 0.01)
    #ax.scatter(x,y,c=bothvals[:,3])
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel('Stop codons per MSA residue')
    ax.set_ylabel('n_queryhits')
    ax.legend()
    #cid = fig.canvas.mpl_connect('button_press_event', onclick)
    #plt.savefig('scatters/Scatter_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype), dpi = 400)
    #plt.close()
    plt.show()
    pdb.set_trace()
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    #ax.plot(x,y, 'o', c = bothvals[:,3])
    ax.scatter(x1,xperres1, label = 'Swissprot', s = 0.01)

    ax.scatter(x2,xperres2, label = 'AntiFam hits (TREMBL)', s = 0.01)
    #ax.scatter(x,y,c=bothvals[:,3])
    ax.set_xlabel('Stop codons per MSA residue (10aa border tolerance)')
    ax.set_ylabel('Fractions of seqs with at least one Stop (10aa border tolerance)')
    #cid = fig.canvas.mpl_connect('button_press_event', onclick)
    plt.savefig('scatters/otherScatter_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype), dpi = 400)
    plt.close()


    fig = plt.figure()
    ax  = fig.add_subplot(111)
    bins = np.linspace(0,0.8, 200)
    ax.hist(x1, bins, alpha=0.5,label = 'SP')
    ax.hist(x2, bins, alpha=0.5,label = 'AF')
    ax.set_ylim([0,20])
    ax.legend()
    ax.set_xlabel('Fraction of sequences with at least one undesired STOP')
    ax.set_ylabel('Count')
    n_empty_af = np.sum(afvals[:,2] == 0)
    n_empty_sp = np.sum(spvals[:,2] == 0)
    ax.set_title('Swissprot and Trembl/Antifam hits. Empty seqs: SP:{} AF:{}'.format(n_empty_sp, n_empty_af))
    plt.savefig('hists/Hist_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff,sptype))
    plt.close()
   
    fig = plt.figure()
    ax  = fig.add_subplot(111)
    bins = np.linspace(0,0.8, 200)
    ax.hist(xperres1, bins, alpha=0.5,label = 'SP')
    ax.hist(xperres2, bins, alpha=0.5,label = 'AF')
    ax.set_ylim([0,20])

    ax.legend()
    ax.set_xlabel('Fraction of STOP codons in body aligment')
    ax.set_ylabel('Count')
    n_empty_af = np.sum(afvals[:,2] == 0)
    n_empty_sp = np.sum(spvals[:,2] == 0)
    ax.set_title('Swissprot and Trembl/Antifam hits. Empty seqs: SP:{} AF:{}'.format(n_empty_sp, n_empty_af))
    plt.savefig('hists/tolborderHist_{}_{}_{}_{}.png'.format(spname,afname, eval_cutoff, sptype))
    plt.close()

    #afvals[score, nfriends,qlen, score2]
    aflens = afvals[:,2]
    splens = spvals[:,2]
    fig = plt.figure()
    plt.hist(aflens[aflens < 1000], bins=100, label="Antifam", alpha = 0.5)
    plt.hist(splens[splens < 1000], bins=100, label='Swissprot', alpha = 0.5)
    plt.legend()
    plt.title('Distribution of protein length')
    plt.xlabel('Sequence length')
    plt.ylabel('Number of sequences')
    plt.savefig('Hists')
    
    np.save('aflens.npy',aflens)
    #Write short summary to infos/
    outfile = 'infos/summary_'+sptype+'_'+eval_cutoff+'.txt'
    with open(outfile, 'a') as logf:
        logf.write('Summary of simple_classifier output.start: {} stop: {} type:{} eval:{}\n\n'.format(rstart, rstop, sptype, eval_cutoff))
        logf.write('Run on {}\n\n'.format(time.strftime("%Y-%m-%d %H:%M")))

        logf.write('Number of SP sequences: {}\n'.format(len(spvals)))
        logf.write('Number of AF sequences: {}\n'.format(len(afvals)))
        logf.write('Number of empty SP sequences: {}\n'.format(n_empty_sp))
        logf.write('Number of empty AF sequences: {}\n'.format(n_empty_af))

        logf.write('AUC: {}\n'.format(round(roc_auc,4)))
        logf.write('AUC perres: {}\n'.format(round(roc_auc2,4)))
        logf.write('AUC len: {}\n'.format(round(roc_auc3,4)))
        logf.write('\n\n')
#benchmark

#for evals in range(10):
#for evals in [0,1,2,3,4,5,6,7,8,9,10]:
evals = sys.argv[1]
use_saved = 0

for i in [0]:
    sptypes = ['spafs','spafs-red','spafs-nored','full-spafs-nored','sp', 'spafs-filter', 'sp-filter']
    aftypes = ['af','af-red','af-nored','full-af-nored','af-filter']
    sptype = sptypes[-1]
    spname = sptypes[-1] 
    afname = aftypes[-1]

    rstart, rstop = 0.1,0.9
    starts = [0.0,0.1,0.2,0.3]
    stops = [0.7,0.8,0.9,1]
    starts = [0.1]
    stops = [0.9]
    for k in range(len(starts)):
        for j in range(len(stops)):
            rstart = starts[k]
            rstop = stops[j]
            print('-----------')
            print(spname, afname)
            run_classifier(spname, afname, rstart, rstop, use_saved = use_saved, benchmark = 0, eval_cutoff =  str(evals), sptype = sptype)


#rois = np.zeros((5,5))
#for n,rstop in enumerate([0.6,0.7,0.8,0.9,1]):#
#    for m,rstart in enumerate([0,0.1,0.2,0.3,0.4]):
#        print(m,n,rstart,rstop)
#        rois[m,n] = run_classifier(rstart, rstop, use_saved = 0, benchmark = 1, specialname = 'ecut10-2')
#pdb.set_trace()
