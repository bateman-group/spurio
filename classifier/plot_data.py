# This script is just used for visualisation and is not
# used by any part of the program

import matplotlib.pyplot as plt
from train_test_cv import prepare_train_test_data
from gpm_model import helperfunctions


def main():
    X, y, names = prepare_train_test_data()
    X = helperfunctions.add_pseudocount(X)
    X = helperfunctions.keep_desired_features(X)
    X = helperfunctions.logit(X)
    idx0 = (y == 0)
    idx1 = (y == 1)

    ax = plt.subplot(111)
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    for axis in ['bottom', 'left']:
            ax.spines[axis].set_linewidth(2)
    plt.ylabel("log(Protein length (amino acids)), normalised")
    plt.xlabel('log(Stop codons per MSA residue), normalised')
    ax.scatter(X[idx1, 0], X[idx1, 1], s=7, label='Swissprot', alpha=0.5)
    ax.scatter(X[idx0, 0], X[idx0, 1], s=7, label='Antifam', alpha=0.5)
    plt.legend()
    plt.show()
    ax = plt.subplot(111)

    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    for axis in ['bottom', 'left']:
        ax.spines[axis].set_linewidth(2)
    plt.ylabel("log(Number of tblastn hits), normalised")
    plt.xlabel('log(Stop codons per MSA residue), normalised')
    ax.scatter(X[idx1, 0], X[idx1, 2], s=7, label='Swissprot', alpha=0.5)
    ax.scatter(X[idx0, 0], X[idx0, 2], s=7, label='Antifam', alpha=0.5)
    plt.legend()
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(X[idx1, 0], X[idx1, 1], X[idx1, 2], s=7, label='Swissprot', alpha=0.5)
    ax.scatter(X[idx0, 0], X[idx0, 1], X[idx0, 2], s=7, label='Antifam', alpha=0.5)
    ax.set_ylabel("log(TBLASTN hits), normalised")
    ax.set_xlabel('log(Stop codons per MSA residue), normalised')
    ax.set_zlabel('log(Sequence length), normalised')
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
