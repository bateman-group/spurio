import numpy as np
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF

from . import helperfunctions

def gpm_train(X, y, outname='None' , save=0):
    '''
    This function is to be called on its own. No direct
    integration into pipeline.

    Input: - numpy array of true proteins, dimensions n_proteins x 4:
             Accession Name, Stops_per_msa, Homologous seqs, Sequence length
           - The same for false proteins, usually derived from antifam.

    Output:  - clf.np: Gaussian process model (sklearn class) and min_ and
               max_ values for (de-normalisation)
             - training error
    '''

    # Preprocess data: Pseudocount, Log, Normalise
    X = helperfunctions.add_pseudocount(X)
    X = helperfunctions.keep_desired_features(X)
    X = helperfunctions.logit(X)
    max_all = np.max(X, axis=0)
    min_all = np.min(X, axis=0)
    X = helperfunctions.normalise(X, min_all, max_all)

    # Train GPM
    kernel = RBF([1])
    clf = GaussianProcessClassifier(kernel=kernel).fit(X, 1 - y)
    if save:
        np.save('trained_model/{}.npy'.format(outname), [clf, min_all, max_all])

    # Return trained model, along with min and max for normalisation
    # testing data
    return clf, min_all, max_all


if __name__ == "__main__":

    spdata = helperfunctions.load_data('sp')
    afdata = helperfunctions.load_data('af')

    OUTDIR = 'trained_model'
    outname = 'clf2'
    clf, min_, max_ = gpm_train(spdata, afdata, outname, save=1)
    print('Saving trained model to {}.'.format(outname))
