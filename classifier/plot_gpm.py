import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np
from gpm_model import helperfunctions


def plot_gpm(clf, min_, max_, X, y=None, save_fig=False):
    '''
    This function takes a trained classifier and data points
    with or without labels, and plots n x n slices of the 3d
    space.
    '''
    # Prepare
    nrows = 3
    ncols = 3

    seqlen_col = 2
    homseq_col = 1
    stops_col = 0

    grid_density = 0.03

    puffer_x = 0.2
    puffer_y = 0.2
    puffer_z = 0

    max_prot_len = 1500

    # Preprocess data #
    X = helperfunctions.preprocess_data(X, min_, max_)

    # Determine x, y and z ranges for the plots
    n_zslices = nrows * ncols

    x_min, x_max = X[:, stops_col].min() - puffer_x, X[:, stops_col ].max() + puffer_x
    y_min, y_max = X[:, homseq_col].min() - puffer_y, X[:, homseq_col].max() + puffer_y
    z_min, z_max = X[:, seqlen_col].min() - puffer_z, X[:, seqlen_col].max() + puffer_z

    # We only want to include proteins up to a certain length.
    zmax_substitute = helperfunctions.normalise(np.array(
        [1, 1, np.log(max_prot_len)]), min_, max_)[0][-1]
    z_max = zmax_substitute

    # Prepare a meshgrid to display the Gaussian classificator
    zslices_lim = np.linspace(z_min, z_max, n_zslices + 1)
    zslices_mid = zslices_lim[:-1] + (np.diff(zslices_lim) / 2)
    xx, yy, zz = np.meshgrid(np.arange(x_min, x_max, grid_density),
                             np.arange(y_min, y_max, grid_density),
                             zslices_mid)

    probs_grid = clf.predict_proba(np.c_[xx.ravel(), yy.ravel(), zz.ravel()])
    probs_grid = probs_grid.reshape((xx.shape[0], xx.shape[1], xx.shape[2], 2))

    zslices_lim_unnorm = helperfunctions.unnormalise(zslices_lim, min_, max_)

    if y is not None:
        pos_idx = (y == 0)
        neg_idx = (y == 1)

    ### PLOT ###
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols)

    for i in np.arange(len(zslices_lim)-1):
        ax = axes.flat[i]

        # Plot colors and isoclines
        ax.imshow(probs_grid[:,:,i,0], extent=(x_min, x_max, y_min, y_max),
                  origin="lower", interpolation='bilinear', vmin=0, vmax=1,
                  alpha=0.8, zorder=2)

        CS = ax.contour(np.arange(x_min, x_max, grid_density),
                        np.arange(y_min, y_max, grid_density),
                        (probs_grid[:, :, i, 0] - 0.5) * 2,
                        colors='black', origin='lower',
                        levels=[-0.98, -0.8, 0, 0.8, 0.98], vmin=-1,
                        vmax=1, linewidths=0.5, zorder=6)

        CS.levels = (CS.levels / 2) + 0.5
        plt.clabel(CS, inline=1, fontsize=5)
        idx_this_length = ((X[:, seqlen_col] > zslices_lim[i]) &
                           (X[:, seqlen_col] < zslices_lim[i + 1]))

        # Add scatter plots
        if y is not None:
            pos_rightlen = pos_idx & idx_this_length
            neg_rightlen = neg_idx & idx_this_length

            ax.scatter(X[pos_rightlen, stops_col], X[pos_rightlen, homseq_col],
                       edgecolor=(0, 0, 0), linewidth=0.1, s=10, zorder=4,
                       c='red')
            ax.scatter(X[neg_rightlen, stops_col], X[neg_rightlen, homseq_col],
                       edgecolor=(0, 0, 0), linewidth=0.1, s=10, zorder=5,
                       c='blue')

        else:
            ax.scatter(X[idx_this_length, stops_col],
                       X[idx_this_length, homseq_col], c='black',
                       edgecolors=(0, 0, 0), s=1, zorder=5)

        # Title
        #TODO Format this better
        ttl = (ax.set_title(str(int(np.exp(zslices_lim_unnorm[i])))[0:4] +
                            ' to ' +
                            str(int(np.exp(zslices_lim_unnorm[i + 1])))[0:4] +
                            ' aa', fontsize=6))
        ttl.set_position([0.5, 0.95])


        # Fiddle with labels
        if i == 3:
            ax.set_ylabel('Homologous DNA sequences (normalised)')
        if i == 7:
            ax.set_xlabel('STOP codons per amino acid (normalised)')
        if i in {1, 2, 4, 5}:
            ax.set_xticks([])
            ax.set_yticks([])
        elif i in {0, 3}:
            ax.set_xticks([])
            ax.tick_params(labelsize=8)
        elif i in {7, 8}:
            ax.tick_params(labelsize=8)
            ax.set_yticks([])
        else:
            ax.tick_params(labelsize=8)

    # Fiddle with sizes

    plt.subplots_adjust(wspace=-0.4, hspace=0.15)
    fig.subplots_adjust(right=0.90)
    cbar_ax = fig.add_axes([0.85, 0.15, 0.02, 0.7])
    m = cm.ScalarMappable()
    m.set_array([0, 0.5, 1])
    cbar = fig.colorbar(m, cax=cbar_ax, ticks=[0, 0.25, 0.5, 0.75, 1])
    cbar.set_label('Probability spurious', rotation=90, labelpad=5)
    cbar.ax.tick_params(labelsize=8)

    #TODO what if our username is not wulf?
    if save_fig:
        plt.savefig('/home/wulf/gauss_process/auto2/{}_{}_{}.png'.format(trainon, ploton, add))
        plt.close()
    else:
        plt.show()
