# Spurio

We have developed the Spurio tool to help identify spurious protein
predictions in prokaryotes.  Spurio searches the query protein sequence against
a prokaryotic nucleotide database using `tblastn` and identifies homologous
sequences.  Features of the matches are used to score the query sequences'
likelihood to be a spurious protein prediction using a Gaussian process model.

This is a project from the Bateman Lab, European Bioinformatics Institute, Hinxton. See the associated publication for more details:

**[H�ps W, Jeffryes M and Bateman A. Gene Unprediction with Spurio: A tool to identify 
spurious protein sequences \[version 1; referees: 2 approved\]. _F1000Research_ 2018, 7:261
(doi: 10.12688/f1000research.14050.1)](https://f1000research.com/articles/7-261/v1)**


## Installation & Setup

To set up Spurio, follow the steps below:

* Clone git repository:

   `git clone --depth 10 https://bitbucket.org/bateman-group/spurio.git`
 
* Change to the repository directory:

  `cd spurio`

* Download bacterial databases, and extract. Decompressing the archive will take some time.

 `wget https://www.ebi.ac.uk/~agb/Spurio/db.tar.gz`
 
 `tar -xvf db.tar.gz`


* Install dependencies

    **Spurio requires Python version 3**. It is also recomended that some form of virtual
    environment is used for running Spurio, to avoid conflicts with other Python applications.

    * Install Python packages using either Conda or Pip

        #### Conda (recommended)
        * `conda install --file requirements.txt`

        #### Pip
        * `pip install -r requirements.txt`

    * Install [bedtools v2.26.0][1]
    
    *In the event of issues running Spurio after installation with Pip, we recomend installing 
    [Miniconda][2] and following the Conda
    instruction above.*

 [1]: http://bedtools.readthedocs.io/en/latest/content/installation.html
 [2]: https://conda.io/miniconda.html

## Usage

Spurio is run by calling spurio.py

Optional Arguments:

  - `-h`, `--help`: Display the help message.
  - `-s START`, `--start START`: The first sequence from the FASTA file to
  analyse (default `1`).
  - `-e END`, `--end END`: The last sequence from the FASTA file to analyse
  (default `1`).
  - `-v EVALUE`, `--evalue EVALUE`: `-log10(expectation value)` used in
  homology search (default `1`).
  - `-r SOURCE`, `--source SOURCE`: The bacteria database (default
  `db/fullsource_filter.fa`).
  - `-q QUERY`, `--query QUERY`: Path to FASTA file containing query
  sequence(s). Only the sequences from START to END inclusive will be read.
  - `-qt QUERYTYPE`, `--querytype QUERYTYPE`: The output filename to be used
  for files created by Spurio (default `query`).


## Output

Spurio writes several files for each protein query.

  - `/output/summaries/`  List of accession names and spurious probabilities
  - `/output/figs/`       Visualisation of results from homology search       
  - `/output/error_logs`  List of problems encountered during analysis

For deeper manual inspection, also:

  - `/output/blast/`: Raw results of tblastn homology search
  - `/output/bedrequests/`: sequences appended to the borders of tblastn hits
  - `/output/nuc_seqs/`: sequences appended to the borders of tblastn hits
  - `/output/np/`: Spurio results as .npy files, useful for
  second or separate classification



## Example run

To test your installation, you can do a sample run:

`python spurio.py`

Computation time: usually around 1-2 minutes.
Results will be written to `output/`


## Re-run Classification

A summary of every homology search is saved in `/output/np`.
If you want to re-run classification or test your own classifier,
refer to `classifier/README` for instructions.

## License

This project is licensed under the MIT license. For more information see the
LICENSE file.
